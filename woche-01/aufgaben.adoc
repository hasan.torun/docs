= Aufgaben
:sectnums:
:stem: latexmath

Aufgaben 1 bis 11 sind eher kleinere Aufgaben, mit denen Sie Ihr Theorie-Verständnis vertiefen können, während Sie in den Aufgaben 12 bis 15 grössere und interessantere Programme schreiben soll. Beide Aufgabenarten sind wichtig!

Die letzten drei Aufgaben werden gemeinsam im EIPR-Unterricht gelöst.

== Strings und Kommentare (Vorlesung)

Entscheiden Sie für folgende Code-Stücke, ob sie gültig sind und, falls ja, welche Ausgabe sie erzeugen:

[loweralpha]
. `+System.out.println(/*"Hi"*/);+`
. `+System.out.println("println()");+`
. `+// System.out.println("Hi");+`
. `+/**/ System.out.println("//Hi");+`
. `+/* System.out.println("Hi */");+`

== Zeichnen mit der Turtle (Vorlesung)

Schreiben Sie je ein Turtle-Programm, welches folgende Zeichnungen produziert:

[loweralpha]
. image:res/turtle-e.png[width=150]
. image:res/turtle-star.png[width=200]

*Hinweise:* Verwenden Sie folgenden `import`-Befehl ganz am Anfang des Programms (vor `public class`):

[source,java]
----
import static ch.trick17.turtle4j.TurtleGraphics.*;
----

Beachten Sie ausserdem, dass die Turtle-Bibliothek nur in dem bereitgestellten Vorlage-Projekt zur Verfügung steht; wenn Sie ein eigenes Projekt in IntelliJ erstellen, können Sie die Turtle-Befehle nicht (ohne weiteres) verwenden.


== Welcher Datentyp passt? (Vorlesung)

Welchen Datentyp würden Sie zum Speichern von folgenden Informationen verwenden?

[loweralpha]
. eine Temperaturmessung in °C
. die Anzahl Kinder in einer Familie
. die durchschnittliche Anzahl Kinder pro Paar
. die Antwort auf: «Bist du schwanger?»
. den Geldbetrag auf Ihrem Konto
. die Anzahl Bäume auf der Erde

== Variablennamen wählen

Welche Variablennamen würden Sie für die Informationen in der vorherigen Aufgabe verwenden?

== Gültige Variablennamen

Welches sind gültige Variablennamen? Versuchen Sie, die Antworten zuerst ohne Hilfsmittel zu geben und überprüfen Sie sie abschliessend, indem Sie ein kleines Programm schreiben, welches Variablen mit diesen Namen enthält, und beobachten, ob der Compiler Fehler meldet.

[loweralpha]
. `yes`
. `firstName`
. `last-name`
. `println`
. `"hello"`
. `for`
. `42isTheAnswer`
. `sum_of_data`
. `_foo`
. `b4`
. `I_AM_GROOT`
. `$money$`
. `System.out`
. `unprotected`

== Ausdrücke lesen

Werten Sie folgende Ausdrücke von Hand aus und überprüfen Sie anschliessend, indem Sie sie ein kleines Programm schreiben, das diese mit `System.out.println(...)` ausgibt.

[loweralpha]
. `23 / 7`
. `2*3 + 6/4`
. `2+3 * 4-6`
. `(12 + 3) / 4 * 2`
. `720310 / 10 / 10 / 10 / 10`
. `- 10 - 20 - - 40`
. `5.0 / 2 * 3 / 2`
. `10.0 / 2 / 4`
. `1.5 + 3 * 1.5`
. `53 / 5 / (0.6 + 1.4) / 2 + 13 / 2`

== Der Modulo-Operator

Der Modulo-Operator `%` berechnet den Rest einer Ganzzahl-Division, z. B. 19 ÷ 5 = 3, *Rest 4.* Werten Sie folgende Ausdrücke von Hand aus und überprüfen Sie wieder mit `println(...)`-Ausgaben.

[loweralpha]
. `17 % 6`
. `300 % 60`
. `917 % 100`
. `(238 % 10 + 3) % 2`
. `26 % 10 % 4 * 3`
. `177 % 100 % 10 / 2`
. `1 + 10 % 10 + 1`

== Programme mit Variablen lesen (Vorlesung)

Welchen Wert haben die Variablen `zahl` und `x` nach jeder der folgenden Anweisungen?

[source,java]
----
int zahl = 1;
int x = 3;
zahl = x * 5;
x = 20;
zahl = zahl + 3;
zahl = x + zahl * 2;
----

== Programme mit Variablen lesen

Welchen Wert haben die Variablen `i`, `j` und `k` nach folgendem Stück Code?

[source,java]
----
int i = 2, j = 3, k = 4;
int x = i + j + k;
i = x - i - j;
j = x - j - k;
k = x - i - k;
----

== Von der Formel zum Ausdruck

Sie haben eine Variable mit der reellen Zahl stem:[x]. Schreiben Sie einen Ausdruck, der stem:[y] berechnet:

[stem]
++++
y=9.1x^3+19.3x^2−4.6x+34.2
++++

*Zusatzaufgabe:* Versuchen Sie, einen Ausdruck zu finden, der _nur drei_ `*`&#8209;Operatoren (und einige `+`) verwendet.

== Doppelte Berechnungen

Ändern Sie folgendes Programm so ab, dass es keine Berechnung mehrmals durchführt. Verwenden Sie Variablen mit geeigneten Typen und Namen um die Resultate von Berechnungen zwischenzuspeichern.

[source,java]
----
System.out.println("Arbeitsstunden:");
System.out.println(4 + 5 + 8 + 4);

System.out.println("Stundenlohn:");
System.out.println("25.50 CHF");

System.out.println("Lohn total:");
System.out.println((4 + 5 + 8 + 4) * 25.50);

System.out.println("Steuern:"); // 20% Steuern...
System.out.println((4 + 5 + 8 + 4) * 25.50 * 0.20);
----


== Vom Quadrat zum Kreis (Vorlesung)

[loweralpha]
. Schreiben Sie ein Turtle-Programm mit einer `for`-Schleife, das ein Quadrat mit einem Umfang von 600 zeichnet.
. Ändern Sie das Programm so ab, dass es ein Sechseck mit dem gleichen Umfang zeichnet.
. Ändern Sie das Programm wieder ab, so dass es ein 13-Eck zeichnet, wiederum mit Umfang 600. Versuchen Sie, das Programm so zu schreiben, dass man zum Ändern der Anzahl Ecken in Zukunft nur eine einzige Stelle im Code ändern muss.
. Ändern Sie das Programm ein letztes Mal, so dass ein 360-Eck gezeichnet wird. Was stellen Sie fest?


== Anhalteweg

Der _Anhalteweg_ ist die Strecke, die ein Fahrzeug vom Zeitpunkt des Erblicken eines Hindernisses bis zum Stillstand zurücklegt. Der Anhaltweg setzt sich aus dem _Reaktionsweg_ und dem _Bremsweg_ zusammen:

[stem]
++++
Anhalteweg = Reaktionsweg + Bremsweg
++++

Reaktionsweg und Bremsweg wiederum lassen sich (vereinfacht) mit folgenden Formeln berechnen (Reaktions- und Bremsweg in Metern, Geschwindigkeit in km/h):

[stem]
++++
\begin{align}
Reaktionsweg &= 3 \times \frac{Geschwindigkeit}{10}\\
Bremsweg &= \left(\frac{Geschwindigkeit}{10}\right)^2
\end{align}
++++

Schreiben Sie ein Programm `Anhalteweg`, welches eine Variable `geschwindigkeit` mit einem bestimmten Wert initialisiert und für diese Geschwindigkeit den Reaktionsweg, Bremsweg und Anhalteweg berechnet und auf der Konsole ausgibt. Ändern Sie die Initialisierung von `geschwindigkeit`, um verschiedene Werte zu testen.


== Bankomat

Vervollständigen Sie das Programm `Bankomat` in Ihrem persönlichen Git-Repository, welches für einen bestimmten Geldbetrag berechnen soll, wie viele Noten von welcher Art der Bankomat ausgibt; dabei sollen immer so wenige Noten wie möglich ausgegeben werden. Ein etwaiger Restbetrag, der nicht in Noten ausbezahlt werden kann, soll am Schluss ebenfalls ausgegeben werden.

**Beispiel:** Für einen Betrag von 3573 CHF soll die Ausgabe des Programms so aussehen:
----
Willkommen bei der Bank Ihres Vertrauens
****************************************

Sie wollen also 3573 CHF abheben.
3 1000er
2 200er
1 100er
1 50er
1 20er
0 10er

Restbetrag: 3 CHF.
----

Vervollständigen Sie das Programm so, dass es für beliebige (nicht-negativen) Beträge die richtige Notenmischung berechnet. Ersetzen Sie dazu den `// TODO`-Kommentar mit eigenem Code. Ändern Sie auch mal die Initialisierung der Variable `betrag` am Anfang des Programms, um verschiedene Beträge zu testen.

*Hinweis:* Für dieses Programm würde sich im Prinzip eine Schleife anbieten, aber mit den Mitteln, die wir bis jetzt kennen, ist das nicht möglich. Stören Sie sich nicht daran, wenn Sie einiges an dupliziertem Code schreiben müssen.

.*Tipp*
[%collapsible]
====
Denken Sie an den Modulo-Operator `%`. Lösen Sie ggf. zuerst die Übung «Der Modulo-Operator».
====


== Turtle und `for`-Schleifen

Schreiben Sie ein Turtle-Programm, welches folgende Zeichnung produziert:

image:res/turtle-oop.png[width=300]

*Hinweise:*

- Um die Turtle zu bewegen ohne zu zeichnen, können Sie die Befehle `penUp();` und `penDown();` verwenden: Nach einem `penUp` zeichnet die Turtle nichts mehr beim Bewegen; nach einem `penDown` zeichnet sie wieder.
- Sie können die Schildkröte schneller laufen lassen, indem Sie am Anfang der `main`-Methode `setSpeed(1000);` ausführen.


== Fibonacci

Die _Fibonacci-Zahlen_ sind eine Folge von Zahlen, bei welcher die ersten beiden Elemente 1 sind und jedes folgende Element die Summe der beiden vorherigen ist. Die ersten 12 Fibonacci-Zahlen lauten:

[stem]
++++
1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144
++++

Schreiben Sie ein Programm mit einer `for`-Schleife, welche die ersten 20 Fibonacci-Zahlen berechnet und ausgibt. Wie viele Variablen (zusätzlich zur Schleifen-Variable) brauchen Sie wohl?


== Quadrat (gemeinsame EIPR-Aufgabe)

Öffnen Sie das Programm `Quadrat` im Ordner `eipr` in Ihrem Repository und ändern sie es so ab, dass über eine Variable gesteuert werden kann, wie gross das Quadrat ist. Gehen Sie dabei wir folgt vor:

[loweralpha]
. Deklarieren Sie eine Variable (`Datentyp name;`)
. Initialisieren Sie die Variable
. Verwenden Sie die Variable um die Grösse (Seitenlänge) des Quadrats zu verändern.


== Rechnung  (gemeinsame EIPR-Aufgabe)

Betrachten Sie das Programm `Rechnung` in Ihrem Repository. Wenn Sie das Programm starten, wird eine Spirale gezeichnet. Sie sollen das Programm wie folgt abändern:

[loweralpha]
. Vergrössern Sie die "Lücken" zwischen den einzelnen Runden.
. Erhöhen Sie die Geschwindigkeit der Schildkröte entsprechend der Länge des Weges die sie gehen muss. Auf kurzen Strecken
soll sie langsamer unterwegs sein. Je länger die Strecken werden um so schneller soll sie laufen.


== Männchen (gemeinsame EIPR-Aufgabe)

Starten Sie das Programm `Maennchen` in Ihrem Repository und besprechen sie es *zu zweit*.

Zeichnen Sie nun jeweils ein eigenes Bild (Strichmännchen, Tier, Haus, Strommast, ...) auf Papier. Tauschen Sie die Zeichnungen aus und schreiben Sie dann ein Programm, welches die Zeichnung des jeweils anderen digitalisiert.